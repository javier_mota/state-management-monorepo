# Getting started


### Install and configure yarn
```npm install --global yarn```

```yarn config set workspaces-experimental true```

### Install dependencies
```yarn install```

### Start project
```lerna run start --parallel```
