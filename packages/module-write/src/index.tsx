import React from 'react';
import { ADD_PRODUCT_MUTATION } from './gql';
import { useMutation } from 'query-client';

function App() {
  const { mutate } = useMutation(ADD_PRODUCT_MUTATION);

  return (
      <form onSubmit={(e) => {
        e.preventDefault();
        const title = (e.currentTarget.title as any).value as string;
        const price = (e.currentTarget.price as any).value as string;
        mutate({input: {
          title,
          price: parseFloat(price)
        }});
      }}>
        <input name="title" type="text" />
        <input name="price" type="number" />
        <input type="submit" value="Submit" />
      </form>
  );
}

export default App;
