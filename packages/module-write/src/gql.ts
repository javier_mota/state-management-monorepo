import { gql } from "query-client";

export const ADD_PRODUCT_MUTATION = gql`
  mutation AddProduct($input: AddProductInput) {
    addProduct(input: $input) {
      id
      title
      price
    }
  }
`
