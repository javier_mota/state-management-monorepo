import { gql as gqlGraphqlRequest } from "graphql-request";
import { composeTransforms } from "./transforms";
import { Gql } from "./types";

export const gql: Gql = (...args) => {
  return composeTransforms({
    directives: {},
    doc: gqlGraphqlRequest(...args),
    toString() {
      return this.doc;
    }
  })
};
