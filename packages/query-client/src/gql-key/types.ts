export interface GqlKey {
  directives: {
    // update?: {
    //   [k in string]: boolean;
    // };
    reactive?: boolean;
    client?: boolean;
  };
  doc: string;
  toString: () => string;
}

export type Gql = (docStr: TemplateStringsArray, ...keys: unknown[]) => GqlKey;
export type GqlKeyTransform = (gqlKey: GqlKey) => GqlKey;
