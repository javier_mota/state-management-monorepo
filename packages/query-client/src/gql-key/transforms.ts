import merge from "lodash/merge";
import { GqlKeyTransform } from "./types";

const reactiveTransform: GqlKeyTransform = (gqlKey) => {
  const reactive = gqlKey.doc.includes("@reactive");
  const doc = gqlKey.doc.replaceAll("@reactive", "");
  return merge(gqlKey, {
    directives: { reactive },
    doc,
  });
};

const clientTransform: GqlKeyTransform = (gqlKey) => {
  const client = gqlKey.doc.includes("@client");
  const doc = gqlKey.doc.replaceAll("@client", "");
  return merge(gqlKey, {
    directives: { client },
    doc,
  });
};

export const transforms: GqlKeyTransform[] = [reactiveTransform, clientTransform];

export const composeTransforms: GqlKeyTransform =
  (gqlKey) =>
    transforms.reduce((key, transform) => transform(key), gqlKey);
