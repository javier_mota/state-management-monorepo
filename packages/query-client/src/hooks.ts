import { useCallback } from "react";
import {
  useQuery as useRQuery,
  useMutation as useRQMutation,
  useQueryClient,
} from "react-query";
import { fetcher } from "./fetcher";
import { GqlKey } from "./gql-key/types";
import { isGqlObg, MutationConfig, Variables } from "./types";
export const useBroadcast = () => {
  const queryClient = useQueryClient();

  return useCallback(() => {
    queryClient.invalidateQueries({
      predicate(query) {
        const queryKey = query.queryKey[0];
        return (
          query.isActive() && isGqlObg(queryKey) && !!queryKey.directives.reactive
        );
      },
    });
  }, [queryClient]);
};

export function useQuery<TData, TVars extends Variables = undefined>(
  query: GqlKey,
  variables?: TVars
) {
  return useRQuery<TData, unknown, TData, [GqlKey, TVars | undefined]>(
    [query, variables],
    () => fetcher(query, variables)
  );
}

export function useMutation<
  TData = unknown,
  TError = unknown,
  TVariables = void
>(
  queryMutation: GqlKey,
  { broadcast = true, invalidateQueries }: MutationConfig = {}
) {
  const queryClient = useQueryClient();
  const emitBroadcast = useBroadcast();
  const { mutate: rqMutate, ...rest } = useRQMutation<
    TData,
    TError,
    TVariables
  >(
    (variables: any) => {
      return fetcher(queryMutation, variables);
    },
    {
      onSuccess() {
        if (broadcast) {
          emitBroadcast();
        }
        if (invalidateQueries) {
          queryClient.invalidateQueries(invalidateQueries);
        }
      },
    }
  );

  const mutate = useCallback(
    (variables) => {
      rqMutate(variables);
    },
    [rqMutate]
  );

  return { mutate, ...rest };
}
