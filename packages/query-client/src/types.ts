import { GqlKey } from "./gql-key/types";


export type Variables = {
  [key: string]: any;
} | undefined;

export interface MutationConfig {
  broadcast?: boolean;
  invalidateQueries?: GqlKey[];
}

export const isGqlObg = (p: unknown): p is GqlKey => {
  if (typeof p === "object" && !!p) {
    return "directives" in p && "doc" in p;
  }
  return false;
}
