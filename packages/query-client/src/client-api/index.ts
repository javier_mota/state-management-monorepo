import { graphql, buildSchema } from "graphql";
import faker from "faker";

var schema = buildSchema(`
  type Hello {
    name: String
  }
  type Query {
    hello: Hello
  }
`);

var root = {
  hello: {
    name: () => {
      return `Hello ${faker.name.firstName()}!`;
    },
  },
};

export const clientQuery = (query: string) => graphql(schema, query, root).then(({data}) => data);
