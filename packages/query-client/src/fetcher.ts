import { request } from "graphql-request";
import { clientQuery } from "./client-api";
import { GqlKey } from "./gql-key";
import { Variables } from "./types";

export const fetcher = (query: GqlKey, variables?: Variables) => {
  if (query.directives.client) {
    return clientQuery(query.doc);
  }
  return request("http://localhost:4000", query.doc, variables);
}
