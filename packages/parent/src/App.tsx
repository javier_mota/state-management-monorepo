import React from 'react';
import './App.css';
import ModuleRead from "module-read";
import ModuleWrite from "module-write";

function App() {

  return (
    <div className="App">
      <ModuleRead />
      <ModuleWrite />
    </div>
  );
}

export default App;
