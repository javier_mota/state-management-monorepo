export const PRODUCT_QUERY = `
  query Products {
    products {
      id
      title
      price
    }
  }
`

export const ADD_PRODUCT_MUTATION = `
  mutation AddProduct($input: AddProductInput) {
    addProduct(input: $input) {
      id
      title
      price
    }
  }
`
