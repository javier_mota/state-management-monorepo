import { gql } from "query-client";

export const CORE_PRODUCT_FIELDS = gql`
  fragment CoreProductFields on Product {
    id
    title
    price
  }
`;

export const PRODUCT_QUERY = gql`
  ${CORE_PRODUCT_FIELDS}
  query Products { @reactive
    products {
      ...CoreProductFields
    }
  }
`;

export const HELLO_WORLD_QUERY = gql`
  query HelloWorld { @client @reactive
    hello {
      name
    }
  }
`;
