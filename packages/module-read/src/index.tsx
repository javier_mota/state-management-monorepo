import { HELLO_WORLD_QUERY, PRODUCT_QUERY } from './gql';
import { useQuery } from 'query-client';

function App() {
  const { data } = useQuery<{products: {id: string; title: string; price: number}[]}>(PRODUCT_QUERY);
  const { data: clientData } = useQuery<{hello: {name: string}}>(HELLO_WORLD_QUERY);

  return (
    <div style={{
      display: "grid",
      gridTemplateColumns: "1fr 1fr"
    }}>
      <div>
        {data?.products.map(({id, title, price}) => <div key={id}>{title}: {price}</div>)}
      </div>
      <div>Client Query: {clientData?.hello.name}</div>
    </div>
  );
}

export * from "./gql";

export default App;
