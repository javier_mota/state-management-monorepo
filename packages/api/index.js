import { ApolloServer, gql } from "apollo-server";
import { join, dirname } from "path";
import { Low, JSONFile } from "lowdb";
import {nanoid} from "nanoid";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));
const file = join(__dirname, "db.json");
const adapter = new JSONFile(file);
const db = new Low(adapter);

const typeDefs = gql`
  type Product {
    id: String
    title: String!
    price: Float!
  }
  input AddProductInput {
    title: String!
    price: Float!
  }

  type Query {
    products: [Product]!
  }

  type Mutation {
    addProduct(input: AddProductInput): Product!
  }
`;

const resolvers = {
  Query: {
    products: (_parent, _args, { db }) => db.data.products,
  },
  Mutation: {
    addProduct: async (_parent, { input }, { db }) => {
      const product = {
        ...input,
        id: nanoid(),
      }
      db.data.products.push(product);
      await db.write();
      return product;
    },
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async () => {
    await db.read();
    db.data = db.data || { products: [] };

    return { db };
  },
});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
